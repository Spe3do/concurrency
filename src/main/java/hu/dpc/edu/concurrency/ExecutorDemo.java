package hu.dpc.edu.concurrency;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorDemo {
    public static void main(String[] args) {
        final ExecutorService executorService = Executors.newFixedThreadPool(2);

        final MyStack stack = new ConcurrentMyStack();
        final ShutdownManager shutdownManager = new DefaultShutdownManager();

        shutdownManager.addExecutorService(executorService);


        final Producer producer = new Producer(stack, shutdownManager);
        final Consumer consumer = new Consumer(stack, shutdownManager);

        final Future<?> producerFuture = executorService.submit(producer);
        executorService.submit(consumer);
        executorService.submit(producer);
        executorService.submit(consumer);

        final List<Runnable> runnables = executorService.shutdownNow();
    }
}
