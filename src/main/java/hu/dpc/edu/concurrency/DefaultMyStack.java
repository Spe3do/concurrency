package hu.dpc.edu.concurrency;

import java.util.NoSuchElementException;

public class DefaultMyStack implements MyStack {

    private char[] array = new char[10];
    private int index = 0;

    public void push(char c) {
        final String tn = Thread.currentThread().getName();
        System.out.println(tn + " push(" + c + ")");
        if (index == array.length) {
            throw new StackFullException();
        }
        array[index++] = c;
    }

    public char pop() {
        final String tn = Thread.currentThread().getName();
        System.out.println(tn + " pop()");
        if (index == 0) {
            throw new NoSuchElementException();
        }
        final char c = array[--index];
        System.out.println("popped: " + c);
        return c;
    }
}

