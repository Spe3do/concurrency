package hu.dpc.edu.concurrency;

public class MyStackTester {
    public static void main(String[] args) throws InterruptedException {
        final MyStack stack = new ConcurrentMyStack();
        final ShutdownManager shutdownManager = new DefaultShutdownManager();


        final Producer producer = new Producer(stack, shutdownManager);
        final Consumer consumer = new Consumer(stack, shutdownManager);

        final Thread p1 = new Thread(producer, "P1");
        final Thread p2 = new Thread(producer, "P2");
        final Thread c1 = new Thread(consumer, "C1");
        final Thread c2 = new Thread(consumer, "C2");



        p1.start();
        p2.start();
        c1.start();
        c2.start();

        shutdownManager.addWorkerThread(p1);
        shutdownManager.addWorkerThread(p2);
        shutdownManager.addWorkerThread(c1);
        shutdownManager.addWorkerThread(c2);


        Thread.sleep(2000);

        shutdownManager.shutdown();

        System.out.println("main ends...");

    }
}
